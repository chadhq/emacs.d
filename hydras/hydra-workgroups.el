(require 'workgroups2)
(require 'projectile)

(defun current-wg () (wg-workgroup-name (wg-current-workgroup)))

(defun hydra-workgroups (i x) (format "_%d_ %s" (+ 1 i) x))

(defun workgroups-list () (-map-indexed 'hydra-workgroups (wg-workgroup-names)))

(defun concatted () (mapconcat 'identity  (workgroups-list) ""))

(defvar num 1)
(defvar cur-color "#40e0d0")

(defun current-wg-name () (if (wg-current-workgroup 'noerror)
                              (wg-workgroup-name  (wg-current-workgroup 'noerror))
                             "Not currently in a workgroup"))


(defhydra hydra-workgroups (:color pink :hint nil)
    "
     ^^WG Enabled         %`workgroups-mode
     ^^Current Workgroup  %(current-wg-name)
     ^^Current Project     %(projectile-project-name)

    ------------------------
    _s_   switch to workgroup          _1_ %(nth 0 (wg-workgroup-names))
                                     ^^_2_ %(nth 1 (wg-workgroup-names))
    _c_   create workgroup             _3_ %(nth 2 (wg-workgroup-names))
    _R_   rename workgroup             _4_ %(nth 3 (wg-workgroup-names))
    _k_   kill workgroup               _5_ %(nth 4 (wg-workgroup-names))
    _!_   reset                        _6_ %(nth 5 (wg-workgroup-names))
                                     ^^_7_ %(nth 6 (wg-workgroup-names))
    _S_   save session                 _8_ %(nth 7 (wg-workgroup-names))
    _L_   load session                 _9_ %(nth 8 (wg-workgroup-names))
                                     ^^_0_ %(nth 9 (wg-workgroup-names))
    _r_   Revert
    _u_   Update

    _q_ quit
"
     ("c" wg-create-workgroup)
     ("R" wg-rename-workgroup)
     ("k" wg-kill-workgroup)
     ("s" wg-switch-to-workgroup :exit truex)
     ("S" wg-save-session)
     ("L" wg-open-session )
     ("r" wg-revert-workgroup :exit truex)
     ("u" wg-update-workgroup :exit truex)
     ("!" wg-reset)
     ("1" (wg-switch-to-workgroup-at-index 0) :exit truex)
     ("2" (wg-switch-to-workgroup-at-index 1) :exit truex)
     ("3" (wg-switch-to-workgroup-at-index 2) :exit truex)
     ("4" (wg-switch-to-workgroup-at-index 3) :exit truex)
     ("5" (wg-switch-to-workgroup-at-index 4) :exit truex)
     ("6" (wg-switch-to-workgroup-at-index 5) :exit truex)
     ("7" (wg-switch-to-workgroup-at-index 6) :exit truex)
     ("8" (wg-switch-to-workgroup-at-index 7) :exit truex)
     ("9" (wg-switch-to-workgroup-at-index 8) :exit truex)
     ("0" (wg-switch-to-workgroup-at-index 9) :exit truex)

     ("1" 'current-wg)

     ("q" nil :exit truex))

(global-set-key (kbd "s-/") 'hydra-workgroups/body)

(provide 'hydra-workgroups)
