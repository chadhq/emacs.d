;;(set-face-attribute 'default nil :height 120)
;; "Essential PragmataPro" -> default face?
;; "Droid Sans Mono"

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "unknown" :family "Essential PragmataPro")))))

;; (put 'downcase-region 'disabled nil)
;; (put 'upcase-region 'disabled nil)
;; (setq inhibit-splash-screen t)
;; (setq inhibit-startup-message t)
;; (setq visible-bell 1)
;; ;; (setq ring-bell-function 'ignore)
;; (when (fboundp 'tool-bar-mode)
;;   (tool-bar-mode 0))
;; (show-paren-mode t)

(defalias 'yes-or-no-p 'y-or-n-p)
(linum-mode 1)
;; (setq inhibit-startup-message t) ;; No splash screen
;; (setq initial-scratch-message nil) ;; No scratch message

;; (recentf-mode 1)
;; (global-set-key (kbd "C-x C-r") 'recentf-open-files)


;; Create backup files in .emacs-backup instead of everywhere
(defvar user-temporary-file-directory "~/.emacs-backup")
(make-directory user-temporary-file-directory t)
(setq backup-by-copying t)
(setq backup-directory-alist
      `(("." . ,user-temporary-file-directory)
	(,tramp-file-name-regexp nil)))
(setq auto-save-list-file-prefix
      (concat user-temporary-file-directory ".auto-saves-"))
(setq auto-save-file-name-transforms
      `((".*" ,user-temporary-file-directory t)))


; make emacs play nicely withthe X11 clipboard
(setq x-select-enable-clipboard t)
;;(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)

; make todo.txt open in org mode automatically
;; (setq auto-mode-alist
;;   (append 
;;    ;; File name (within directory) starts with a dot.
;;    '(
;;      ;; File name end with todo.txt
;;      ("todo\\.txt\\'" . org-mode))
;;    auto-mode-alist))


(defvar cdorrat/packages '(			   
			   ag
			   cl
			   clj-refactor
			   command-log-mode
			   company
			   anzu
			   helm
			   helm-core
			   cljr-helm
			   helm-cider
			   helm-cider-history
			   helm-projectile
			   hydra
			   key-chord
			   magit
			   markdown-mode
			   multiple-cursors
			   projectile
			   ace-jump-mode
			   cider
			   dash
			   dockerfile-mode
			   ess
			   fiplr
			   fixmee
			   git-gutter-fringe
			   helm-ag
			   iedit
			   itail
			   jump-char
			   nxml
			   package
			   paredit
			   paredit-menu
			   s
			   sass-mode
			   tramp
			   workgroups2
			   wsd-mode
			   yaml-mode
			   expand-region
			   cyberpunk-theme
			   undo-tree
			   project-explorer
			   neotree
			   string-inflection
			   fringe-helper
			   git-gutter-fringe+
			   nyan-mode
			   color-theme-wombat
			   color-theme-solarized
			   color-theme-tango
			   smart-mode-line
			   smart-mode-line-powerline-theme
			   restclient))

(require 'cl)
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/") t)
;; (add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)
;; (add-to-list 'package-pinned-packages '(clojure-mode . "melpa-stable") t)
(package-initialize)


(defun cdorrat/packages-installed-p ()
  (loop for pkg in cdorrat/packages
        when (not (package-installed-p pkg)) do (return nil)
        finally (return t)))

(unless (cdorrat/packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (dolist (pkg cdorrat/packages)
    (when (not (package-installed-p pkg))
      (package-install pkg))))

(defconst user-init-dir
  (cond ((boundp 'dotfiles-dir) dotfiles-dir) 
	((boundp 'user-emacs-directory) user-emacs-directory)
        ((boundp 'user-init-directory) user-init-directory)
        (t "~/.emacs.d/")))

(setq modules-path (file-name-as-directory (concat user-init-dir  "modules")))
(add-to-list 'load-path modules-path)

(add-hook 'after-init-hook 
	  (lambda () (load-theme 'cyberpunk t)))

(add-hook 'after-init-hook 
	  (lambda () (hydra-workgroups/body)))


(require 'tramp)
(require 'iedit) ;; C-; search/replace


;; =================================================================================================== 
;; auto completion
(setq tab-always-indent 'complete)
(add-to-list 'completion-styles 'initials t)

(setq ido-enable-flex-matching t)
(ido-mode 1)
(ido-everywhere 1)

(require 'company)
(global-company-mode)

(recentf-mode 1)
(setq recentf-max-menu-items 100)

;; =================================================================================================== 
;; Configure clojure
(require 'my-clojure)
(require 'setup-paredit)
(require 'paredit-menu)

(require 'comment-sexp)
(global-set-key (kbd "M-;") 'comment-or-uncomment-sexp)

(require 'ob)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((clojure . t)
   (sh . t)
   (emacs-lisp . t)
   (sql . t)))

(setq org-babel-clojure-backend 'cider)

;; Let's have pretty source code blocks
(setq org-edit-src-content-indentation 0
      org-src-tab-acts-natively t
      org-src-fontify-natively t
       org-confirm-babel-evaluate nil
      org-support-shift-select 'always)

;; Useful keybindings when using Clojure from Org
(org-defkey org-mode-map "\C-x\C-e" 'cider-eval-last-sexp)
(org-defkey org-mode-map "\C-c\C-d" 'cider-doc)

;; No timeout when executing calls on Cider via nrepl
(setq org-babel-clojure-nrepl-timeout nil)

(require 'emacs-rotate)

(show-paren-mode 1)

(delete-selection-mode 1)


;; ===================================================================================================
;; smart-mode-line

;(smart-mode-line-enable)
(rich-minority-mode 1)
(setq sml/no-confirm-load-theme t)
(setq sml/theme 'dark)

(add-to-list 'rm-blacklist " ClojureScript")
(add-to-list 'rm-blacklist " ARev")
(add-to-list 'rm-blacklist " Anzu")
(add-to-list 'rm-blacklist " Paredit")
(add-to-list 'rm-blacklist " company")
(add-to-list 'rm-blacklist " Undo-Tree")
(add-to-list 'rm-blacklist " wg")
(add-to-list 'rm-blacklist " GitGutter")
(add-to-list 'rm-blacklist " cljr")
(add-to-list 'rm-blacklist " yas")

(sml/setup)


;; ===================================================================================================
;; Undo Tree
(global-undo-tree-mode)

;; ===================================================================================================
;; jump-char

(require 'fast-load)
(global-set-key [(f8)] 'xah-open-file-fast)


(require 'ace-jump-mode)
(require 'jump-char)

(global-set-key [(meta m)] 'jump-char-forward)
(global-set-key [(shift meta m)] 'jump-char-backward)
(global-set-key (quote [67108912]) 'ace-jump-mode) ;; Ctrl-0
(global-unset-key "")

;;==================
;; String inflection

(require 'string-inflection)
(global-set-key (kbd "M-|") 'string-inflection-all-cycle)


;;
;; eshell
;;(local-set-key [C-up] (quote eshell-previous-matching-input-from-input))
(require 'eshell)
(add-hook 'eshell-mode-hook
	  '(lambda ()
	     (define-key eshell-mode-map [C-up] 'eshell-previous-matching-input-from-input)))




(defun eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
	     (current-buffer))
    (error (message "Invalid expression")
	   (insert (current-kill 0)))))

(defun my-curr-buffer-to-cider ()
  (interactive)
  (pop-to-buffer-same-window   
   (cider-current-repl-buffer)))

;; ANZU Mode 
(require 'anzu)
(global-anzu-mode +1)
(set-face-attribute 'anzu-mode-line nil
                    :foreground "yellow" :weight 'bold)

(define-key isearch-mode-map [remap isearch-query-replace]  #'anzu-isearch-query-replace)
(define-key isearch-mode-map [remap isearch-query-replace-regexp] #'anzu-isearch-query-replace-regexp)

;; Projctile


(require 'projectile)
(require 'helm-projectile)
(projectile-global-mode)
(setq projectile-completion-system 'helm)
;;(setq projectile-completion-system 'grizzl)
(helm-projectile-on)


;;(setq projectile-mode-line '(:eval (format " P[%s]" (projectile-project-name))))
(setq projectile-mode-line '(:eval (format "")))

(setq hydra-path (file-name-as-directory (concat user-init-dir  "hydras")))
(add-to-list 'load-path hydra-path)
(require 'hydra-cljr)
(require 'hydra-windmove)
(require 'hydra-projectile)
(require 'hydra-workgroups)
(require 'hydra-mc)
(require 'hydra-er)
(workgroups-mode 1)

;; (require 'hydra-cljr)
;; (require 'hydra-cljr)			
;; (require 'hydra-cljr)


;;TREE VIEW - project-explorer
;;(setq pe/project-root-function 'projectile-project-root)
;;(global-set-key [f10] 'project-explorer-open)
;;TREE VIEW - neotree
(require 'neotree)
(defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root."))))
(global-set-key [f8] 'neotree-project-dir)




(when (eq system-type 'darwin)
      (set-default-font "-*-Hack-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1"))

(defun top-join-line ()
  "Join the current line with the line beneath it."
  (interactive)
  (delete-indentation 1))


(defun my-curr-buffer-to-cider ()
  (interactive)
  (pop-to-buffer-same-window   
   (cider-current-repl-buffer)))

(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "M-W") 'fixup-whitespace)

(global-set-key [f11] 'toggle-frame-fullscreen)
(global-set-key [f10] 'magit-status)

(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "M-+") 'text-scale-decrease)

(global-set-key (kbd "C-=") 'er/expand-region)
(global-set-key (kbd "C--") 'er/contract-region)

;; (define-key clojure-mode-map (kbd "C-c M-j") 'cider-jack-in)
;; (define-key clojure-mode-map (kbd "C-c M-J") 'cider-restart)

;; (define-key paredit-mode-map [s-down] 'paredit-forward-down)
;; (define-key paredit-mode-map [s-right] 'paredit-forward-up)
;; (define-key paredit-mode-map [s-left] 'paredit-backward-down)
;; (define-key paredit-mode-map [s-up] 'paredit-backward-up)

;;(define-key paredit-mode-map (kbd "s-,") 'backward-sexp)
;;(define-key paredit-mode-map (kbd "s-.") 'forward-sexp)

(global-set-key [M-s-up] 'windmove-up)
(global-set-key [M-s-down] 'windmove-down)
(global-set-key [M-s-left] 'windmove-left)
(global-set-key [M-s-right] 'windmove-right)

(global-set-key (kbd "C-M-<") 'shrink-window-horizontally)
(global-set-key (kbd "C-M->") 'enlarge-window-horizontally)
(global-set-key (kbd "C-M-\"") 'shrink-window)
(global-set-key (kbd "C-M-:") 'enlarge-window)

;;(global-set-key (kbd "C-c C-e") 'ac-complete-file)

;;Magit
;; (global-set-key (kbd "C-c g d") 'magit-diff)
;; (global-set-key (kbd "C-c g l") 'magit-log)
;; (global-set-key (kbd "C-c g e") 'magit-ediff)
;; (global-set-key (kbd "C-c g d") 'magit-diff)
;; (global-set-key (kbd "C-c g c") 'magit-commit)
;; (global-set-key (kbd "C-c g p") 'magit-push)

;; (global-set-key (kbd "C-x C-g") 'rgrep)

;; Show-hide
;(global-set-key (kbd "C-c =") 'hs-show-block)
;(global-set-key (kbd "C-c +") 'hs-show-all)
;(global-set-key (kbd "C-c -") 'hs-hide-block)
;(global-set-key (kbd "C-c _") 'hs-hide-all)

(global-set-key [M-left] 'previous-buffer)
(global-set-key [M-right] 'next-buffer)

(global-set-key (kbd "C-^") 'top-join-line)
(global-set-key (kbd "C-%") 'join-line)

;;HELM SHORTCUTS
(global-set-key (kbd "s-b") 'helm-projectile-switch-to-buffer)
(global-set-key (kbd "s-e") 'helm-projectile-recentf)
(global-set-key (kbd "s-o") 'projectile-find-file)
(global-set-key (kbd "s-f") 'projectile-find-file)
(global-set-key (kbd "s-p") 'projectile-switch-project)
(global-set-key (kbd "s-[") 'helm-projectile-ag)

(global-set-key (kbd "s-E") 'helm-recentf)
(global-set-key (kbd "s-B") 'helm-buffers-list)

(global-set-key (kbd "s-w") 'hydra-window/body)

;;HELM
(global-set-key (kbd "M-x") 'helm-M-x)
;;(global-set-key (kbd "s-p") 'helm-projectile-switch-project)
;;(global-set-key (kbd "s-e") 'helm-projectile-recentf)
;;(global-set-key (kbd "s-b") 'helm-projectile-switch-to-buffer)
;;(global-set-key (kbd "s-o") 'helm-projectile-find-file)


;;IEDIT
(global-set-key (kbd "C-;") 'iedit-mode)

;;WIND MOVE
(global-set-key (kbd "C-M-<") 'shrink-window-horizontally)
(global-set-key (kbd "C-M->") 'enlarge-window-horizontally)
(global-set-key (kbd "C-M-\"") 'shrink-window)
(global-set-key (kbd "C-M-:") 'enlarge-window)


(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-.") 'mc/mark-next-like-this)
(global-set-key (kbd "C-,") 'mc/mark-previous-like-this)
(global-set-key (kbd "C->") 'mc/mark-all-like-this)
(global-set-key (kbd "C-<") 'mc/mark-all-like-this)



;; (defun duplicate-line()
;;   (interactive)
;;   (move-beginning-of-line 1)
;;   (kill-line)
;;   (yank)
;;   (open-line 1)
;;   (next-line 1)
;;   (yank))

;; (global-set-key (kbd "M-s-d") 'duplicate-line)

;; (global-set-key [(control shift up)] 'move-line-up)
;; (global-set-key [(control shift down)] 'move-line-down)



(set-variable
 'exec-path
 (quote
 ("/usr/bin" "/bin" "/usr/sbin" "/usr/local/bin" "/sbin" "/Users/chad/bin"
  "/Applications/Emacs.app/Contents/MacOS/bin-x86_64-10_9"
  "/Applications/Emacs.app/Contents/MacOS/libexec-x86_64-10_9"
  "/Applications/Emacs.app/Contents/MacOS/libexec"
  "/Applications/Emacs.app/Contents/MacOS/bin")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#000000" "#8b0000" "#00ff00" "#ffa500" "#7b68ee" "#dc8cc3" "#93e0e3" "#dcdccc"])
 '(custom-safe-themes
   (quote
    ("84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "d6922c974e8a78378eacb01414183ce32bc8dbf2de78aabcc6ad8172547cb074" default)))
 '(fci-rule-color "#383838"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )






;; ;;;GIT GUTTER FRINGE
(require 'git-gutter-fringe+)
(git-gutter+-mode 1)
(setq-default left-fringe-width  5)
(setq-default right-fringe-width 5)

(set-face-foreground 'git-gutter-fr+-modified "yellow")
(set-face-foreground 'git-gutter-fr+-added    "green")
(set-face-foreground 'git-gutter-fr+-deleted  "red")

(fringe-helper-define 'git-gutter-fr+-added nil
  "x.x.x"
  ".."
  "x.x.x"
  ".."
  "x.x.x"
  ".."
  "x.x.x"
  "..")


(fringe-helper-define 'git-gutter-fr+-deleted nil
  "x.x.x"
  "..."
  "x.x.x"
  "..."
  "x.x.x"
  "..."
  "x.x.x"
  "...")

(fringe-helper-define 'git-gutter-fr+-modified nil
  "x.x.x"
  "..."
  "x.x.x"
  "..."
  "x.x.x"
  "..."
  "x.x.x"
  "...")
